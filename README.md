# Automação de Testes em Python - Demo Automation Testing

Este repositório contém scripts de automação de testes para as funcionalidades do site [Demo Automation Testing](https://demo.automationtesting.in/). Foram implementados testes para as seguintes funcionalidades:

- Register
- DatePicker
- Slider
- Frames

## Pré-requisitos

Para executar os testes de automação, você precisa ter o seguinte software instalado em seu ambiente de desenvolvimento:

- [Python](https://www.python.org/downloads/)
- [Selenium WebDriver](https://www.selenium.dev/documentation/en/webdriver/driver_requirements/)
- [WebDriver executável](https://www.selenium.dev/documentation/en/webdriver/driver_requirements/#quick-reference)
- [Git](https://git-scm.com/downloads)

Certifique-se de que todas as variáveis de ambiente necessárias estejam configuradas corretamente.

## Configuração do Projeto

1. Clone este repositório para o seu ambiente local usando o seguinte comando:

```
git clone https://gitlab.com/reniton/demoautomationtestingpython.git
```

2. Navegue até o diretório do projeto:

```
cd automacao-demo-automationtesting
```

3. Instale as dependências necessárias usando o pip:

```
pip install -U pytest
pip install selenium
```

4. Execute os testes usando o seguinte comando na raiz do projeto:

```
pytest -v 
```

Os testes serão executados automaticamente e você poderá ver os resultados no console.

## Funcionalidades Automatizadas

### Register

O script de automação para a funcionalidade de Register realiza o preenchimento do formulário de registro com dados fictícios e verifica se o registro foi concluído com sucesso.

### DatePicker

O script de automação para a funcionalidade de DatePicker abre o calendário de seleção de data, escolhe uma data específica e verifica se a data selecionada está correta.

### Slider

O script de automação para a funcionalidade de Slider move a barra deslizante para uma posição específica e verifica se a barra foi movida corretamente.

### Frames

O script de automação para a funcionalidade de Frames interage com os frames na página e executa ações específicas dentro de cada frame.

## Tecnologias Utilizadas

- [Selenium WebDriver](https://www.selenium.dev/documentation/en/webdriver/): Framework de automação de testes para interagir com páginas da web.
- [Python](https://www.python.org/): Linguagem de programação utilizada para escrever os scripts de automação.
- [Git](https://git-scm.com/): Sistema de controle de versão utilizado para o gerenciamento do código fonte.

## Autor

Este projeto foi desenvolvido por [Reniton Oliveira Feitosa Monteiro](https://gitlab.com/reniton).