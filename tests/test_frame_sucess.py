import time

import pytest

from pages.frame_page import FramePage


@pytest.mark.usefixtures("setup_teardown")
class TestFrameSucess():
    def test_ct01_single_frame(self):
        frame = FramePage()
        frame.inserir_valor_no_iframe("Texto inserido no iframe")
        time.sleep(5)  # Espera para visualizar o resultado


