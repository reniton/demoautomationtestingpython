import time

import pytest

from pages.slider_page import SliderPage


@pytest.mark.usefixtures("setup_teardown")
class TestSliderSucess():
    def test_ct01_slider(self):
        slider = SliderPage()
        slider.selecionar_slider()
        time.sleep(5)  # Espera para visualizar o resultado
