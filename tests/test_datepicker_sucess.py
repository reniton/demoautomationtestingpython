import pytest

import conftest
from pages.datapicker_page import DatapickerPage


@pytest.mark.usefixtures("setup_teardown")
class TestDatepickerSucesso():
    def test_ct01_datepicker_disable(self):
        data = DatapickerPage()
        data.datePicker_disabled("03/20/2024")

    def test_ct01_datepicker_enabled(self):
        data = DatapickerPage()
        data.datePicker_enabled("03/20/2024")