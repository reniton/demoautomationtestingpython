import pytest
from pages.RegisterPage import RegisterPage
import conftest

@pytest.mark.usefixtures("setup_teardown")
class TestRegisterSucesso():
    def test_ct01_register_sucesso(self):
        driver = conftest.driver
        register_page = RegisterPage()
        register_page.preencher_formulario_registro(
            firstname="John",
            lastname="Doe",
            address="123 Street, City, Country",
            email="john.doe@example.com",
            phone="1234567890",
            gender="Male",
            hobbies=["Cricket", "Movies"],
            languages=["English", "Portuguese"],
            country="Brazil",
            skills="Python",
            year="1990",
            month="August",
            day="15",
            password="Test@123"
        )

