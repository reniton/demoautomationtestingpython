from selenium.webdriver.common.by import By
import time
from pages.base_page import BasePage
from pages.menu_page import MenuPage


class DatapickerPage(BasePage):
    def __init__(self):
        super().__init__()
        menu = MenuPage()
        menu.acessar_menu_datepicker()
        self.datepicker_field = (By.ID, "datepicker1")
        self.datepicker_icon = (By.XPATH, "//img[@class='imgdp']")
        self.datepicker_field2 = (By.ID, "datepicker2")

    def datePicker_disabled(self, data):
        # Clicar no ícone do calendário para abrir o seletor de data
        self.clicar(self.datepicker_icon)
        # Selecionar a data no calendário
        self.driver.execute_script(f"document.getElementById('datepicker1').value = '{data}'")

    def datePicker_enabled(self, data):
        # Preencher o campo de texto com a data
        self.encontrar_elemento(self.datepicker_field2).send_keys(data)
