from selenium.webdriver import Keys, ActionChains
from selenium.webdriver.common.by import By

import conftest
from pages.base_page import BasePage
from pages.menu_page import MenuPage


class SliderPage(BasePage):
    def __init__(self):
        super().__init__()
        self.driver = conftest.driver
        menu = MenuPage()
        menu.acessar_menu_slider()
        self.slider = (By.ID, "slider")


    def selecionar_slider(self):
        # Encontrar o elemento do slider
        slider_element = self.encontrar_elemento(self.slider)
        # Obter o tamanho do slider
        slider_width = slider_element.size['width']
        # Calcula o valor correspondente a 50%
        valor_slider = int(slider_width / 2) - slider_width / 2
        # Usando o ActionChains para mover o slider para 50%
        ActionChains(self.driver).click_and_hold(slider_element).move_by_offset(valor_slider, 0).release().perform()
