from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from pages.menu_page import MenuPage


class FramePage(BasePage):
    def __init__(self):
        super().__init__()
        menu = MenuPage()
        menu.acessar_menu_frame()
        self.iframe = (By.XPATH, "//iframe[@name='SingleFrame']")
        self.text_area = (By.XPATH, "//input[@type='text']")

    def inserir_valor_no_iframe(self, value):
        # Mudar para o iframe
        self.driver.switch_to.frame(self.encontrar_elemento(self.iframe))
        # Inserir valor no campo de texto dentro do iframe
        self.escrever(self.text_area, value)
        # Voltar para o contexto padrão (fora do iframe)
        self.driver.switch_to.default_content()
