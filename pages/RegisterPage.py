import conftest
import time
from pages.base_page import BasePage
from selenium.webdriver.common.by import By


class RegisterPage(BasePage):

    def __init__(self):
        super().__init__()
        self.driver = conftest.driver
        self.firstname_field = (By.XPATH, "//input[@placeholder='First Name']")
        self.lastname_field = (By.XPATH, "//input[@placeholder='Last Name']")
        self.address_field = (By.XPATH, "//textarea[@ng-model='Adress']")
        self.email_field = (By.XPATH, "//input[@type='email']")
        self.phone_field = (By.XPATH, "//input[@type='tel']")
        self.gender_male_radio = (By.XPATH, "//input[@value='Male']")
        self.hobbies_cricket_checkbox = (By.XPATH, "//input[@value='Cricket']")
        self.hobbies_movies_checkbox = (By.XPATH, "//input[@value='Movies']")
        self.languages_dropdown = (By.ID, "msdd")
        self.country_dropdown = (By.ID, "countries")
        self.country_dropdown_option = (By.XPATH, "//span[@class='select2-selection__arrow']")
        self.country_dropdown_selection = (By.XPATH, "//li[contains(text(),'Japan')]")
        self.skills_dropdown = (By.ID, "Skills")
        self.country_dropdown = (By.ID, "countries")
        self.year_dropdown = (By.ID, "yearbox")
        self.month_dropdown = (By.XPATH, "//select[@placeholder='Month']")
        self.day_dropdown = (By.ID, "daybox")
        self.password_field = (By.ID, "firstpassword")
        self.confirm_password_field = (By.ID, "secondpassword")
        self.submit_button = (By.ID, "submitbtn")

    def preencher_formulario_registro(self, firstname, lastname, address, email, phone, gender, hobbies, languages,
                                      country, skills, year, month, day, password):
        # Preencher o formulário de registro
        self.escrever(self.firstname_field, firstname)
        self.escrever(self.lastname_field, lastname)
        self.escrever(self.address_field, address)
        self.escrever(self.email_field, email)
        self.escrever(self.phone_field, phone)
        if gender.lower() == 'male':
            self.clicar(self.gender_male_radio)
        for hobby in hobbies:
            if hobby.lower() == 'cricket':
                self.clicar(self.hobbies_cricket_checkbox)
            elif hobby.lower() == 'movies':
                self.clicar(self.hobbies_movies_checkbox)
        self.clicar(self.languages_dropdown)
        for language in languages:
            self.clicar((By.XPATH, f"//a[contains(text(),'{language}')]"))
        self.clicar(self.country_dropdown_option)
        self.clicar(self.country_dropdown_selection)
        self.escrever(self.skills_dropdown, skills)
    #    self.clicar(self.country_dropdown)
      #  self.clicar((By.XPATH, f"//li[contains(text(),'{country}')]"))
        self.escrever(self.year_dropdown, year)
        self.escrever(self.month_dropdown, month)
        self.escrever(self.day_dropdown, day)
        self.escrever(self.password_field, password)
        self.escrever(self.confirm_password_field, password)
        self.driver.implicitly_wait(30)
        time.sleep(30)
