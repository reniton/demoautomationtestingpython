from selenium.webdriver.common.by import By

import conftest
from pages.base_page import BasePage


class MenuPage(BasePage):
    def __init__(self):
        super().__init__()
        self.driver = conftest.driver
        self.widgets_menu = (By.XPATH, "//a[contains(text(),'Widgets')]")
        self.datepicker_menu = (By.XPATH, "//a[contains(text(),'Datepicker')]")
        self.switchto_menu = (By.XPATH, "//a[contains(text(),'SwitchTo')]")
        self.frames_submenu = (By.XPATH, "//a[contains(text(),'Frames')]")
        self.slider_submenu = (By.XPATH, "//a[contains(text(),'Slider')]")

    def acessar_menu_datepicker(self):
        # Clicar no menu "Widgets"
        self.clicar(self.widgets_menu)
        # Clicar no submenu "DatePicker"
        self.clicar(self.datepicker_menu)

    def acessar_menu_frame(self):
        # Clicar no menu "Frames"
        self.clicar(self.switchto_menu)
        # Clicar no submenu "DatePicker"
        self.clicar(self.frames_submenu)

    def acessar_menu_slider(self):
        # Clicar no menu "Widgets"
        self.clicar(self.widgets_menu)
        # Clicar no submenu "Slider"
        self.clicar(self.slider_submenu)