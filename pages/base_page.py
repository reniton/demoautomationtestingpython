from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select

import conftest


class BasePage:
    def __init__(self):
        self.driver = conftest.driver

    def encontrar_elemento(self, locator):
        return self.driver.find_element(*locator)

    def encontrar_elementos(self, locator):
        return self.driver.find_elements(*locator)

    def escrever(self, locator, text):
        self.encontrar_elemento(locator).send_keys(text)

    def clicar(self, locator):
        self.encontrar_elemento(locator).click()

    def limpar_e_escrever(self, locator, text):
        elemento = self.encontrar_elemento(locator)
        elemento.clear()
        elemento.send_keys(text)

    def clicar(self, locator):
        self.encontrar_elemento(locator).click()

    def clicar_duplo(self, locator):
        elemento = self.encontrar_elemento(locator)
        ActionChains(self.driver).double_click(elemento).perform()

    def pressionar_tecla(self, locator, tecla):
        self.encontrar_elemento(locator).send_keys(tecla)

    def selecionar_opcao_por_texto_visivel(self, locator, texto_visivel):
        select = Select(self.encontrar_elemento(locator))
        select.select_by_visible_text(texto_visivel)

    def selecionar_opcao_por_valor(self, locator, valor):
        select = Select(self.encontrar_elemento(locator))
        select.select_by_value(valor)

    def selecionar_opcao_por_index(self, locator, index):
        select = Select(self.encontrar_elemento(locator))
        select.select_by_index(index)

    def mover_para_elemento(self, locator):
        elemento = self.encontrar_elemento(locator)
        ActionChains(self.driver).move_to_element(elemento).perform()

    def arrastar_e_soltar(self, source_locator, target_locator):
        source_element = self.encontrar_elemento(source_locator)
        target_element = self.encontrar_elemento(target_locator)
        ActionChains(self.driver).drag_and_drop(source_element, target_element).perform()

    def scroll_para_baixo(self):
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    def scroll_para_cima(self):
        self.driver.execute_script("window.scrollTo(0, 0);")

    def refresh(self):
        self.driver.refresh()

    def aceitar_alerta(self):
        self.driver.switch_to.alert.accept()

    def rejeitar_alerta(self):
        self.driver.switch_to.alert.dismiss()

